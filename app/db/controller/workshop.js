
var mongoose = require('mongoose'),
Workshop = require('../model/workshop'),

Q = require('q')


exports.addWorkshop = function (req, res) {
var deferred = Q.defer()
console.log('createBlock function', req.body)
var workshop = new Workshop(req.body);
workshop.save(function (err, result) {
  if (err) {
    console.log(err)
    deferred.reject(err)
  } else {
    
    res.json(result)
    deferred.resolve(result);
  }
})
return deferred.promise;
}


exports.listWorkshop = function (req, res) {

var deferred = Q.defer()
Workshop.find({}, function (err, result) {
  console.log(result)
  if (err) {
    console.log(err)
    deferred.reject(err)
  } else {
    console.log("inside cards2")
    res.json(result);
    deferred.resolve(result);
  }
})
return deferred.promise;
}
