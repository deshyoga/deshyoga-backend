
var mongoose = require('mongoose'),
Admin = require('../model/admin'),
Q = require('q')

exports.updateAdmin = function (req, res) {
    var deferred = Q.defer()
    Admin.findOneAndUpdate({ password: req.body.password }, {password: req.body.newpassword}, { upsert: true,new: true }, function(err, result) {
         if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        res.json({status:true})
        deferred.resolve({status:true});
      }
    })
    return deferred.promise;
  }