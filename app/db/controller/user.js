
var mongoose = require('mongoose'),
User = require('../model/user'),
Report = require('../model/report'),
  request = require('request'),
Q = require('q')

exports.createUser = function (req, res) {
    var deferred = Q.defer()
    console.log('createBlock function',req.body)
    var user = new User(req.body);
    user.save(function (err, result) {
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        res.json(result)
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }


  exports.listUser = function(req, res) {


    console.log("user_body"+JSON.stringify(req.body))
    
    var deferred = Q.defer()
    User.find({}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        console.log("inside cards2")
        res.json(result);
        deferred.resolve(result);
      }
    }).populate('location.state').populate('location.city').populate('allowance').populate('location.locality')
    return deferred.promise;
  }



  exports.login = function(req, res) {

    var deferred = Q.defer()
    User.find({'userdetails.username':req.body.email,'userdetails.password':req.body.password}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {


console.log("dsjkdbgyyugyugyt"+JSON.stringify(result))

   if(result.length>0){
        var date2 = new Date(); 
        Report.find({mr:result[0]._id}, function(err, report_result) {
          console.log(result)
          if (err) {
          
          } else {

       var Difference_In_Days=0;


        if(report_result.length!=0){
          var Difference_In_Time = date2.getTime()-report_result[0].createdAt.getTime(); 
          
          // To calculate the no. of days between two dates 
           Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
  
          result[0].difference_in_days=Difference_In_Days;
        }

        else{
          Difference_In_Days=0
        }

        var response={
          days:Math.round(Difference_In_Days),
          user:result,
          status:1
        }


        res.status(200);
        res.json(response);
        deferred.resolve(response);


          }
        })
}


else{

  res.json({error:"Incorrect username or password",status:0});

  deferred.resolve(result);
}
     



      }
    })
    return deferred.promise;
  }


  exports.getUserByUsername = function(req, res) {
    
    var deferred = Q.defer()
    User.find({username:req.body.username,password:req.body.password}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        console.log("inside cards2")
        result.status=200;
        res.json(result);
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }

  exports.getUserByUserType = function(req, res) {
    
    var deferred = Q.defer()
    console.log("dsssssss"+req.body.userType)
    // let p=
    User.find({"userdetails.userType":req.body.userType}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        console.log("inside cards2")
        
        result.status=200;
        res.json(result);
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }
  exports.removeUser = function(req, res) {
    
    var deferred = Q.defer()
    User.deleteOne({_id:req.params._id}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        console.log("inside cards2")
        res.json(result);
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }


  exports.updateUser = function (req, res) {

    var deferred = Q.defer()

    

// console.log("ine 178"+JSON.stringify(req.body))

    User.findOneAndUpdate({ _id: req.body._id}, { $set:{userdetails:req.body.userdetails,role:req.body.role,status:req.body.status }}).then((err, result) => {
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        console.log("inside cards2")
        res.json(result);
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }

  exports.getUserByUserTypeAndLocality = function(req, res) {
    
    var deferred = Q.defer()
    console.log("jjkkjjhh"+JSON.stringify(req.body))
    // let p=
    User.find({"userType":req.body.userType,'location.locality': { "$in" : req.body.locality}}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        console.log("inside cards2")
        
        result.status=200;
        res.json(result);
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }


  exports.getMrByManagerID = function(req, res) {
    var deferred = Q.defer()
    User.find({"mrManager":req.body.manager}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        result.status=200;
        res.json(result);
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }


  exports.getUserByUserTypeAndCity = function(req, res) {
    var deferred = Q.defer()
    console.log("jjkkjjhh"+JSON.stringify(req.body))
    // let p=
    User.find({"userType":req.body.userType,'location.city': { "$in" : req.body.city}}, function(err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
        console.log("inside cards2")
        
        result.status=200;
        res.json(result);
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }

  exports.getUserByStateAndUserType = function (req, res) {


    var deferred = Q.defer()
    User.find({ 'location.state':req.body.state,userType:req.body.userType }, function (err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
  
        res.json(result)
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }


  exports.getUserByCityAndUserType = function (req, res) {


    var deferred = Q.defer()
    User.find({ 'location.city':req.body.city,'userdetails[0].userType':req.body.userType }, function (err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
  
        res.json(result)
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }

  exports.getUserByCityAndUserTypeAdmin = function (req, res) {
    var deferred = Q.defer()
    User.find({ 'location.city':{ "$in" : req.body.city},'userType':req.body.userType }, function (err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
  
        res.json(result)
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }

  exports.getMRByLocality = function (req, res) {
    var deferred = Q.defer()
    User.find({ 'location.locality':{ "$in" : req.body.locality},'userType':req.body.userType }, function (err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
  
        res.json(result)
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }


  



  exports.getUserByUserTypeAndState = function (req, res) {
    var deferred = Q.defer()
    User.find({ 'location.state':req.body.state,'userType':req.body.userType }, function (err, result) {
      console.log(result)
      if (err) {
        console.log(err)
        deferred.reject(err)
      } else {
  
        res.json(result)
        deferred.resolve(result);
      }
    })
    return deferred.promise;
  }

  


  exports.getOtp = function (req, res) {
    var deferred = Q.defer()
    request({
      url: "http://ec2-13-232-119-194.ap-south-1.compute.amazonaws.com/api/v1/user/sendotp",
      method: "POST", 
      json:true,  // <--Very important!!!
      body: req.body
  }, function (error, response, body){
      console.log(JSON.stringify(response)+"kjnbhhvdgysvd");

      console.log()
  });
    return deferred.promise;
  }





  // { "$in" : req.body.locality}
  