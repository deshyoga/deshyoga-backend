var mongoose = require('mongoose')

Schema = mongoose.Schema,
    TempdoctorsSchema = new Schema({
        doctordetails: {
            name: { type: String },
            phoneNumber: { type: String },
            email: { type: String },
            dob: { type: String },
            qualification: { type: String },
            gender: { type: String },
            anniversary: { type: String }
        },
        location: {
            state: { type: Schema.Types.ObjectId, ref: 'State' },
            city: { type: Schema.Types.ObjectId, ref: 'City' },
            locality: [{ type: Schema.Types.ObjectId, ref: 'Locality' }],
        },
        addedby:{ type: Schema.Types.ObjectId, ref: 'User' },
        action:{ type: String }

    }, { timestamps: true });

module.exports = mongoose.model('Tempdoctors', TempdoctorsSchema);


