var mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
StateSchema = new Schema({
    name:{ type:String },
    city:[{type: Schema.Types.ObjectId, ref: 'City'}]
},{timestamps: true});

module.exports = mongoose.model('State',StateSchema);