var mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
Level2FeedbackSchema = new Schema({
title:['Level-2 Feedback'],
name: {type:String},
whatsapp_number: {type:String,unique: true},
initial_adjustment:{type:String},
enjoy_journey:{type:String},
per_following_diet_schedule:{type:String},
recommend_deshyoga:{type:String},
efficiency_increase:{type:String},
anger_control:{type:String},
share_data:{type:String},
date_on_which_started_diet:{type:String},
diabetic_before_satvik_diet:{type:String},
effectiveness_addressing_diabetes:{type:String},
bp_problem:{type:String},
effectiveness_addressing_bp:{type:String},
thyroid_issue_before_diet:{type:String},
effectiveness_addressing_thyroid:{type:String},
cholestrol_issue_before_diet:{type:String},
effectiveness_addressing_cholestrol:{type:String},
arthritis_issue_before_diet:{type:String},
effectiveness_addressing_arthritis:{type:String},
weight_loss_21_days:{type:String},
inch_loss_after_21_days:{type:String},
asked_eat_less:{type:String},
effectiveness_addressing_obesity:{type:String},
calmness:{type:String},
comment_satvik_aahar:{type:String},
overall_satsfaction:{type:String},
comment_to_share:{type:String}
    
},{timestamps: true});
Level2FeedbackSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Level2Feedback',Level2FeedbackSchema);