var mongoose = require('mongoose')

Schema = mongoose.Schema,
SubLocalitySchema = new Schema({
    name:{ type:String },
    locality:{
        type: Schema.Types.ObjectId, ref: 'Locality'
    }
  
},{timestamps: true});

module.exports = mongoose.model('SubLocality',SubLocalitySchema);