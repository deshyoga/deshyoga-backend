var mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
YogauserSchema = new Schema({
  
   
        name:{type:String},
        level:{type:String},
        whatsapp_number:{type:String,unique: true},
        age:{type:String},
        // otherdisease:{type:String},
        // thyroid_reading_T3:{type:String},
        // cholesterol_reading_hdl:{type:String},
        // cholesterol_reading_ldl:{type:String},
        // obesity_issue_overweight:{type:String},
        // thyroid_issue:{type:String},
        // cholestrol_issue:{type:String},
        // have_bp_problem:{type:String},
        // arthritis:{type:String},
        // bp_reading_high:{type:String},
        // diabetic_reading_HbA1c:{type:String},
        // cholesterol_reading_VLDL:{type:String},
        // cholesterol_reading_total_tholesterol:{type:String},
        // thyroid_reading_TSH:{type:String},
        // weight_before_starting_diet:{type:String},
        // cholesterol_reading_trglycerides:{type:String},
        // are_you_diabetic:{type:String},
        // diabetic_reading_fasting:{type:String},
        // bp_reading_low:{type:String},
        // thyroid_reading_T4:{type:String},
        // diabetic_reading_PP:{type:String},
        // thyroid_reading_T3RU:{type:String},
        weight_before_starting_diet:{type:String},
        height:{type:String},
        gender:{type:String},
        city:{type:String},
        country:{type:String},
        email:{type:String},
        how_to_know:{type:String},
        profession:{type:String},
        organization:{type:String}
  
            
    
    
},{timestamps: true});
YogauserSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Yogauser',YogauserSchema);