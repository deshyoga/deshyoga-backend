var mongoose = require('mongoose')

Schema = mongoose.Schema,
LocalitySchema = new Schema({
    name:{ type:String },
    city:{
        type: Schema.Types.ObjectId, ref: 'City'
    },
    sublocality:[{
        type: Schema.Types.ObjectId, ref: 'SubLocality'
    }]
  
},{timestamps: true});

module.exports = mongoose.model('Locality',LocalitySchema);