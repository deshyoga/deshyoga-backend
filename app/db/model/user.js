var mongoose = require('mongoose')

Schema = mongoose.Schema,
UserSchema = new Schema({
  
    userdetails:[{
        name:{type:String},
        email:{type:String},
        password:{type:String},
        dateofbirth:{type:String},
        gender:{type:String},
        phoneNumber:{type:String},
        userType:{type:String},
        username:{type:String} ,
        status:{type:String},

    }],
    userType:{type:String},
    mrManager:{type:String},
   
    location: {
        state: { type: Schema.Types.ObjectId, ref: 'State' },
        city:[{ type: Schema.Types.ObjectId, ref: 'City' }],
        locality: [{ type: Schema.Types.ObjectId, ref: 'Locality' }]
    },
    role:{type:String}
    
},{timestamps: true});

module.exports = mongoose.model('User',UserSchema);