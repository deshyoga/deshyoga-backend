var mongoose = require('mongoose')

Schema = mongoose.Schema,
DonationSchema = new Schema({
    name:{ type:String },
    phonenumber:{ type:String },
    amount:{ type:String },
    city:{ type:String },
    country:{ type:String },
    purpose:{ type:String },
},{timestamps: true});

module.exports = mongoose.model('Donation',DonationSchema);