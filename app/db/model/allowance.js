var mongoose = require('mongoose')
dateFormat = require('dateformat');
var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
    AllowanceSchema = new Schema({
        mr: { type: Schema.Types.ObjectId, ref: 'User' },
        state: { type: Schema.Types.ObjectId, ref: 'State' },
        dailyAllowance: { type: Boolean },
        otherAllowance: {
            hotels: [{
                hotelname: { type: String },
                days: { type: Number }
            }]
            ,others: [{
                from: { type: String },
                to: { type: String },
                distance: { type: String }
             
            }]
        },
        travelAllowance: [{
            from: { type: String },
                to: { type: String },
                distance: { type: String }
        }],
        branch:{ type: String },
        userrole:{ type: String },
        TA:{ type: Number },
        DA:{ type: Number },
        OA:{type: Number},
        userType:{type:String},
        date:{type:String,default:dateFormat(new Date(),"dd-mm-yyyy")}
    }, { timestamps: true });

module.exports = mongoose.model('Allowance', AllowanceSchema);


