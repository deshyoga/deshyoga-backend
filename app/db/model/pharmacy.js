var mongoose = require('mongoose')

Schema = mongoose.Schema,
    PharmacySchema = new Schema({
        pharmacydetails: [{
            name: { type: String },
            phoneNumber: { type: String },
            address: { type: String },
            owner: { type: String }
        }],
        location: [{
            city: { type: Schema.Types.ObjectId, ref: 'City' },
            state: { type: Schema.Types.ObjectId, ref: 'State' },
            locality: { type: Schema.Types.ObjectId, ref: 'Locality' }
        }]


    }, { timestamps: true });

module.exports = mongoose.model('Pharmacy', PharmacySchema);