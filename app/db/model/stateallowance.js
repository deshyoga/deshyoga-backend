var mongoose = require('mongoose')

Schema = mongoose.Schema,
StateAllowanceSchema = new Schema({

    allowance:{
        ta:{ type:String}
    },
    state:{type: Schema.Types.ObjectId, ref: 'State',unique: true}
},{timestamps: true});

module.exports = mongoose.model('StateAllowance',StateAllowanceSchema);