var mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
Level4Schema = new Schema({
  
    level:{type:String},
    name:{type:String},
    whatsapp_number:{type:String,unique: true},
    isYogaKit:{type:String,unique: true},
    support:{type:String,unique: true}
      
},{timestamps: true});
Level4Schema.plugin(uniqueValidator);
module.exports = mongoose.model('Level4',Level4Schema);