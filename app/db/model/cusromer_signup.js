var mongoose = require('mongoose')

Schema = mongoose.Schema,
Customer_SignupSchema = new Schema({
    name:{ type:String },
    username:{ type:String },
    password:{ type:String },
    email_address:{ type:String } 
});

module.exports = mongoose.model('Customer_Signup',Customer_SignupSchema);