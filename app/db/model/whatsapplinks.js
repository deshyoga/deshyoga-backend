var mongoose = require('mongoose')

Schema = mongoose.Schema,
WhatsappLinks = new Schema({
    group_id:{ type:String },
    link:{ type:String },
},{timestamps: true});

module.exports = mongoose.model('WhatsappLinks',WhatsappLinksSchema);