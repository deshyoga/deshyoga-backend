var mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
WorkshopSchema = new Schema({
    name:{ type:String },
    phonenumber:{ type:String },
   email:{ type:String },
},{timestamps: true});
WorkshopSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Workshop',WorkshopSchema);