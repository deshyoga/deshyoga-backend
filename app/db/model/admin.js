var mongoose = require('mongoose')

Schema = mongoose.Schema,
AdminSchema = new Schema({
    username:{ type:String },
    password:{ type:String }
    
},{timestamps: true});

module.exports = mongoose.model('Admin',AdminSchema);