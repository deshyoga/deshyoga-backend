var mongoose = require('mongoose')
dateFormat = require('dateformat');
// var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
    ReportSchema = new Schema({
        location: {
            state: { type: Schema.Types.ObjectId, ref: 'State' },
            city: [{ type: Schema.Types.ObjectId, ref: 'City' }],
            locality: [{ type: Schema.Types.ObjectId, ref: 'Locality' }]
        },
        doctors: [{ type: Schema.Types.ObjectId, ref: 'Doctors' }],
        pharmacy: [{ type: Schema.Types.ObjectId, ref: 'Pharmacy' }],
        mr:{ type: Schema.Types.ObjectId, ref: 'User'},
        userType:{type:String},
        isSubmitted:{ type: Boolean, default:false},
        reviews:{type:String},  
        currentLocation:{type:String},
        isVerifiedByAdmin:{ type: Boolean, default:false}, 
        date:{type:String,default:dateFormat(new Date(),"dd-mm-yyyy")},
        withMr:[{ type: Schema.Types.ObjectId, ref: 'User' }],
        allowance:{ type: Schema.Types.ObjectId, ref: 'Allowance' }
    }, { timestamps: true });

module.exports = mongoose.model('Report', ReportSchema);