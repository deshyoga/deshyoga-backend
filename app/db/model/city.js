var mongoose = require('mongoose')

Schema = mongoose.Schema,
CitySchema = new Schema({
    name:{ type:String },
    locality:[{
        type: Schema.Types.ObjectId, ref: 'Locality'
    }]
},{timestamps: true});

module.exports = mongoose.model('City',CitySchema);