var mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
Schema = mongoose.Schema,
Level3FeedbackSchema = new Schema({
  

        email: {type:String},
        feel_after_one_month:{type:String},
        name: {type:String},
        gender: {type:String},
        whatsapp_number: {type:String, unique: true},
        food_served_more_nutritious:{type:String},
        rank_taste:{type:String},
  
        level3_food_removes_toxins_faster:{type:String},
        is_classtime_fine:{type:String},
        deshyoga_should_serve_level3_food_to_all:{type:String},
        level3_class_able_to_arrange_meeting_with_your_better_self:{type:String},
        level3_content_perfect:{type:String},
        goodness_level2_is_1_then_how_good_level3:{type:String},
        level3_food_make_aware:{type:String},
        leve3_control_anger:{type:String},
        share_data:{type:String},
        travel_further:{type:String},
        support_deshyoga:{type:String},
        comment_further_improvement:{type:String},
        benefits_observed:{type:String},



      
  
    
},{timestamps: true});
Level3FeedbackSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Level3Feedback',Level3FeedbackSchema);