var mongoose = require('mongoose')

Schema = mongoose.Schema,
LocationSchema = new Schema({
    name:{ type:String },
    latLng:{type:String}
  
});

module.exports = mongoose.model('Location',LocationSchema);