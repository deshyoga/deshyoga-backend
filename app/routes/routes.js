'use strict';

var login = require('../db/controller/user');
var paypal = require('paypal-rest-sdk');
var admin = require("../db/controller/admin")
var yogausers = require("../db/controller/yogausers")
var level2 = require("../db/controller/level2")
var level3 = require("../db/controller/level3")
var level4 = require("../db/controller/level4")
var verificationcode = require("../db/controller/verification-code")
var level2feedback = require("../db/controller/level2feedback")
var level3feedback = require("../db/controller/level3feedback")
var diabeticuser = require("../db/controller/diabities")
var donation = require("../db/controller/donation")
var workshop = require("../db/controller/workshop")
var paytmCtl = require("../db/controller/payment/payment_paytm")
var rp = require('request-promise')
var user = require("../db/controller/user")
var request = require('request');
const https = require('https');
rp = require('request-promise')
module.exports = function (app) {


  app.get('/', function (req, res) {

    // ejs render automatically looks in the views folder
    res.render('index');
  });

  // app.routes('/user')
  // .post(login.getUserByUsername)

  app.route('/admin')
    .put(admin.updateAdmin)


  app.route('/user')
    .post(user.createUser)
    .get(user.listUser)
    .put(user.updateUser)

  app.route('/user/:_id')
    .delete(user.removeUser)

  app.route('/login')
    .post(user.login)

  app.route('/yogausers')
    .post(yogausers.createUser)
    .get(yogausers.listUser)

  app.route('/level1/:id')
    .delete(yogausers.removeUser)

  app.route('/level2')
    .post(level2.createUser)
    .get(level2.listUser)

  app.route('/level2/:id')
    .delete(level2.removeUser)

  app.route('/level3')
    .post(level3.createUser)
    .get(level3.listUser)

  app.route('/level3/:id')
    .delete(level3.removeUser)

  app.route('/level4')
    .post(level4.createUser)
    .get(level4.listUser)

  app.route('/sendotp')
    .post(verificationcode.sendOTP)

  app.route('/sugar/sendconformation')
    .post(verificationcode.sendConfirmationMessageToSugarStudent)


  app.route('/workshop/jal-niti')
    .post(verificationcode.sendConfirmationMessageToJlNiti)

  app.route('/students/jalniti')
    .post(workshop.addWorkshop)
    .get(workshop.listWorkshop)

  app.route('/sendconformationmsg')
    .post(verificationcode.sendConfirmationMessage)

  app.route('/level3feedback')
    .post(level3feedback.createFeedback)
    .get(level3feedback.listFeedback)

  app.route('/level2feedback')
    .post(level2feedback.createFeedback)
    .get(level2feedback.listFeedback)

  app.route('/donation')
    .post(donation.addDonation)
    .get(donation.listDonation)

  app.route('/sugar-user')
    .post(diabeticuser.createUser)
    .get(diabeticuser.listUser)

  app.route('/sugar-user/:id')
    .delete(diabeticuser.removeUser)

  app.route('/paytm')
    .post(paytmCtl.paytmPayment)

  app.route('/callback')
    .post(paytmCtl.paytmCallback)

  app.route('/jalniti/callback')
    .post(paytmCtl.paytmCallbackJalNiti)

  app.route('/callback/level3')
    .post(paytmCtl.paytmCallbackLevel3)

  app.route('/check-sugar-student')
    .post(diabeticuser.getStudentByPhoneNumber)

  app.route('/getotp')
    .post(user.getOtp)

  app.post('/reversegeocode', function (req, res) {
    var latlong = req.body.lat + ',' + req.body.long
    var options = {
      method: 'GET',
      url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latlong + '&key=AIzaSyA5Ub8STdYDtzfpkXv2yrLqeupAYFmwdtI',
      json: true
    };

    request(options, function (error, response, body) {

      res.send(response)
    });

  })

};
