/* @author Vivek 
 * Dated - 11 dec 2019
 */

//'use strict';

const
  bodyParser = require('body-parser'),
  crypto = require('crypto'),
  express = require('express'),
  https = require('https'),
  request = require('request'),
  moment = require('moment')
//  paypal = require('paypal-rest-sdk');
app = express();
var mongoose = require('mongoose');
mongoose.connect('mongodb://admin:admin123456@ds151626.mlab.com:51626/deshyoga');
// mongoose.connect(process.env.DATABASE)
// CONNECTION EVENTS
var database = mongoose.connection;
database.on('error', console.error.bind(console, 'connection error:'));
database.once('open', function () {
  // we're connected!
  console.log('\n\nConnected!!')
});

var allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Authorization, Accept,Content-Length, X-Requested-With, X-PINGOTHER');
  if ('OPTIONS' === req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
};

app.set('port', process.env.PORT || 3500);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(express.static('public'));

app.use(allowCrossDomain);
var api = require('./app/routes/routes')(app);
app.listen(app.get('port'), function () {
  console.log('Node app is running on port', app.get('port'));
});

module.exports = app;
